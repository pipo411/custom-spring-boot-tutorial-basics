echo "Deploying"

echo "Kill Java Process"
sudo pkill -f java

rm -rf /home/ubuntu/springbootwebapp_deploy
mkdir /home/ubuntu/springbootwebapp_deploy

echo "Start spring-boot-tutorial-basics-1.0.1.jar"
sudo cp -f /tmp/spring-boot-tutorial-basics-1.0.1.jar /home/ubuntu/springbootwebapp_deploy/spring-boot-tutorial-basics-1.0.1.jar
sudo nohup java -jar /home/ubuntu/springbootwebapp_deploy/spring-boot-tutorial-basics-1.0.1.jar &